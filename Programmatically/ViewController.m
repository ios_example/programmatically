//
//  ViewController.m
//  Programmatically
//
//  Created by Minbok.Choi on 2017. 7. 3..
//  Copyright © 2017년 KDS. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
	
	UILabel *helloLabel = [[UILabel alloc] init];
	[helloLabel setFrame:CGRectMake(0.f, 200.f, 320.f, 30.f)];
	[helloLabel setText:@"Hello World!!"];
	[self.view addSubview:helloLabel];
}


- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}


@end
